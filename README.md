# OpenML dataset: Lorenz_attractor_regime_changes

https://www.openml.org/d/42182

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

This dataset contains a simulation of the Lorenz attractor with the parameter $\rho$ varying in time. The stable and chaotic regimes alternate.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/42182) of an [OpenML dataset](https://www.openml.org/d/42182). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/42182/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/42182/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/42182/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

